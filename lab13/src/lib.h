/**
* @file lib.h
* @brief First declaration of function.
* @author Zozulia Igor.
* @date 07-dec-2020
* @version 1.0
*/

#include <string.h>
#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * @param SIZE - size of array;
*/

#define SIZE 37

#ifndef PROGRAMING_LIB_H
#define PROGRAMING_LIB_H

int findCount(char *arr);

#endif //PROGRAMING_LIB_H
