#include "lib.h"

/**
* @file lib.c
* @brief Realization of function.
* @author Zozulia Igor.
* @date 14-dec-2020
* @version 1.0
*/

/**
 * @param arr - input array;
 * @return успішний код повернення з програми (count).
 */

/**
Функція findCount: Find count of words in text.
*/

int findCount(char *arr)
{
	int count = 0;
	char *phc = strtok(arr, " ");
	while (phc != NULL) {
		phc = strtok(NULL, " ");
		count++;
	}
	return count;
}