#include "lib.h"
/**
 * @file main.c
 * @brief Find count of words in text.
 * @author Zozulia Igor.
 * @date 07-dec-2020
 * @version 1.0
 */

/**
 * @param input - input array;
 * @param count - count of words;
 * @return успішний код повернення з програми (0).
 */

/**
Function main: generate text array and send it to function findCount.
*/

int main()
{
	FILE *fp;
	if ((fp = freopen("./assets/input.txt", "r", stdin)) == NULL) {
		printf("%s\n", "Error opening file.");
		exit(1);
	}
	printf("%s %s %s\n", "Laboratory 13.", "Author: Zozulia Igor.", "Find count of words in text.");
	printf("Enter the text: \n");
	char input[256];
	fgets(input, 256, stdin);
	int count = findCount(input);
	printf("Count of words: %d\n", count);
	fclose(fp);
	return 0;
}
