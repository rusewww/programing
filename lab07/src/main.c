/**
 * @file main.c.
 *
 * Laboratory №7. Task: Make function from programs 5 and 6 labs.
 */

/**
 * @param num - given number;
 * @param factorial - double factorial of given number;
 * @param nick - given word;
 * @param SIZE - length of the word;
 */

#include <string.h>
#include <stdlib.h>
#define SIZE 7

int doubleFactorial(int num);
void nickMaker(char *nick);

int main()
{
	int num = 7;
	int result = doubleFactorial(num);
	char *nick = strdup("Pasione");
	nickMaker(nick);
	free(nick);
	return 0;
}

int doubleFactorial(int num)
{
	int factorial;

	if ((num % 2) == 0) {
		factorial = 2;
	} else {
		factorial = 1;
	}
	for (int i = factorial + 2; i <= num; i += 2) {
		factorial = factorial * i;
	}
	return factorial;
}

void nickMaker(char *nick)
{
	for (int i = 0; i < SIZE; i++) {
		if (nick[i] == 'a' || nick[i] == 'A') {
			nick[i] = '@';
		} else if (nick[i] == 'i' || nick[i] == 'I') {
			nick[i] = '1';
		} else if (nick[i] == 'o' || nick[i] == 'O') {
			nick[i] = '0';
		} else if (nick[i] == 's' || nick[i] == 'S') {
			nick[i] = '$';
		}
	}
}
