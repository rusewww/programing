/**
 * @file main.c.
 *
 * Laboratory №4. Task: Check if a triangle exists with given angles.
 */

/**
 * @param r - radius;
 * @param command - command(s - space; l - length; v - void);
 * @param PI - PI number;
 * @param result - show if a triangle exists with given angles(1 - exists; 2 - doesn`t exists);
 */

#define PI 3.14

int main()
{
	const float r = 5;
	char command = 's';
	float result;
	switch (command) {
	case 'l':
		result = 2 * r * (float)PI;
		break;
	case 's':
		result = r * r * (float)PI;
		break;
	case 'v':
		result = 4 * r * r * r / 3 * (float)PI;
		break;
	}
	return 0;
}