#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
* @file lib.h
* @brief First declaration of function.
* @author Zozulia Igor.
* @date 07-dec-2020
* @version 1.0
*/

/**
 *@param SIZE - maximum count of words;
*/

#define SIZE 255

#ifndef PROGRAMING_LIB_H
#define PROGRAMING_LIB_H

/**
 * watch - information about watch;
*/

struct watch {
	char model[SIZE]; /**< model of watch */
	char brand[SIZE]; /**< brand of watch */
	char style[SIZE]; /**< style of watch */
};

void read(char *filename, struct watch *input);

void write(char *filename, struct watch *output);

void showinconsole(struct watch *out);

#endif //PROGRAMING_LIB_H
