#include "lib.h"

/**
* @file lib.c
* @brief Realization of function.
* @author Zozulia Igor.
* @date 07-dec-2020
* @version 1.0
*/

/**
 * @param filename - name of file;
 * @param input - structure for read;
 */

/**
function read: read structure from file.
*/

void read(char *filename, struct watch *input)
{
	FILE *infile;

	infile = fopen(filename, "r");
	if (infile == NULL) {
		printf("%s\n", "Error opening input file.");
		exit(1);
	}

	fscanf(infile, "%99s %99s %99s", input->model, input->brand, input->style);

	fclose(infile);
}

/**
 * @param filename - name of file;
 * @param output - structure for write;
*/

/**
function write: write structure on file.
*/

void write(char *filename, struct watch *output)
{
	FILE *outfile;
	outfile = fopen(filename, "wb");
	if (outfile == NULL) {
		printf("%s\n", "Error opening output file.");
		exit(1);
	}
	fprintf(outfile, "Model: %s\nBrand: %s\nStyle: %s\n", output->model, output->brand, output->style);
	fclose(outfile);
}

/**
 * @param out - structure for show in console;
*/

/**
function showinconsole: show structure in console.
*/

void showinconsole(struct watch *out)
{
	printf("Model: %s\nBrand: %s\nStyle: %s\n", out->model, out->brand, out->style);
}