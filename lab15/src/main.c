#include "lib.h"

/**
 * @file main.c
 * @brief Interaction with the user by the mechanism of input and output from file.
 * @author Zozulia Igor.
 * @date 07-dec-2020
 * @version 1.0
 */

/**
 * @param infilename - input file name;
 * @param first - first structure;
 * @param outfilename - output file name;
 * @return успішний код повернення з програми (0).
 */

/**
Function main: generate structure and call function read, showinconsole,.
*/

int main()
{
	printf("%s\n", "Laboratory 15. Author: Zozulia Igor. Interaction with the user by the mechanism of input and output from file.");
	struct watch first;

	read("./assets/input.txt", &first);

	showinconsole(&first);

	write("./dist/output.txt", &first);
	return 0;
}
