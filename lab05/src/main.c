/**
 * @file main.c.
 *
 * Laboratory №5. Task: Find double factorial.
 */

/**
 * @param num - given number;
 * @param factorial - double factorial of given number;
 */
int main()
{
	int num = 6;
	int factorial = num % 2;
	for (int i = (factorial % 2) + 2; i <= num; i += 2) {
		factorial = factorial * i;
	}
	return 0;
}
