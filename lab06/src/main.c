#define _SVID_SOURCE
#define _POSIX_C_SOURCE 200809L

#include <string.h>
#include <stdlib.h>
/**
 * @file main.c.
 *
 * Laboratory №6. Task: Generate nickname.
 */

/**
 * @param nick - given word;
 * @param SIZE - length of the word;
 */

#define SIZE 7
int main()
{
	char *nick = strdup("Pasione");
	for (int i = 0; i < SIZE; i++) {
		if (nick[i] == 'a' || nick[i] == 'A') {
			nick[i] = '@';
		} else if (nick[i] == 'i' || nick[i] == 'I') {
			nick[i] = '1';
		} else if (nick[i] == 'o' || nick[i] == 'O') {
			nick[i] = '0';
		} else if (nick[i] == 's' || nick[i] == 'S') {
			nick[i] = '$';
		}
	}
	free(nick);
	return 0;
}