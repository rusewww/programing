/**
 * @file main.c.
 *
 * Laboratory №3. Task: Find the sum of numbers between two.
 */

/**
 * @param min - first number;
 * @param max - second number;
 * @param count - count of numbers between two;
 * @param sum - sum of numbers between two;
 */
int main()
{
	int min = 100;
	int max = 110;
	int count = max - min;
	float sum = ((float)(min + max) / 2) * ((float)count + 1);
	return 0;
}
