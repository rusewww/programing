#include "lib.h"
/**
* @file lib.c
* @brief Realization of function.
* @author Zozulia Igor.
* @date 07-dec-2020
* @version 1.0
*/

/**
 * @param arr - array;
 * @param size - array count;
 * @param max - max generation number;
 */

/**
Function randomize: generate random numbers.
*/

void randomize(int arr[], int size, int max)
{
	for (int i = 0; i < size; i++) {
		arr[i] = random() % max + 1;
	}
}

/**
 * @param mass1 - array one;
 * @param mass2 - array two;
 * @param res - result;
 * @param n - size of first;
 * @param m - size of second;
 */

/**
Function combine: combine two arrays.
*/

void combine(int mass1[], int mass2[], int *res, int n, int m)
{
	int j = 0;
	for (int i = 0; i < (n + m); i++) {
		if (i < n) {
			*(res + i) = mass1[i];
		} else {
			*(res + i) = mass2[j];
			j++;
		}
	}
}

/**
 * @param arr - array;
 * @param n - size of first;
 */

/**
Function sort_array: sort array.
*/

void sort_array(int n, int arr[])
{
	for (int i = 0; i < n - 1; i++) {
		for (int j = 0; j < n - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}