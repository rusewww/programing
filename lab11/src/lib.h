/**
* @file lib.h
* @brief First declaration of function.
* @author Zozulia Igor.
* @date 07-dec-2020
* @version 1.0
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/**
 * @param M - first length;
 */
#define M 5

/**
 * @param N -second length;
 */

#define N 4

/**
 * @param MAX - max number;
 */

#define MAX 20

#ifndef PROGRAMING_LIB_H
#define PROGRAMING_LIB_H

void randomize(int arr[], int size, int max);

void combine(int mass1[], int mass2[], int *res, int n, int m);

void sort_array(int n, int arr[]);
#endif //PROGRAMING_LIB_H
