#include "lib.h"
/**
 * @file main.c
 * @brief Combine and sort two arrays.
 * @author Zozulia Igor.
 * @date 07-dec-2020
 * @version 1.0
 */

/**
 * @param mass1 - first array;
 * @param mass2 - second array;
 * @param res - result array;
 * @return успішний код повернення з програми (0).
 */

/**
Function main: generate two arrays by randomize and send data to combine and sort_array.
*/

int main()
{
	int mass1[M];
	randomize(mass1, M, MAX);
	int mass2[N];
	randomize(mass2, N, MAX);
	int *res = (int *)malloc((unsigned int)(N + M) * sizeof(int));

	combine(mass1, mass2, res, N, M);

	sort_array((N + M), res);

	for (int i = 0; i < N + M; i++)
		printf("%d\n", res[i]);
	free(res);
	return 0;
}