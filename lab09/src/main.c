#include <stdlib.h>
#include <string.h>
/**
 * @file main.c
 * @return успішний код повернення з програми (0).
 * @brief Make function.
 * @author Zozulia Igor.
 * @date 07-dec-2020
 * @version 1.0
*/

int doubleFactorial(int num);
void nickMaker(char *nick);

/**
 * @param SIZE - length of word;
*/

#define SIZE 7

/**
 * @param num - given number;
 * @param factorial - double factorial of given number;
 * @param nick - given word;
 * @return успішний код повернення з програми (0).
*/

/**
Function main: generate two arrays by randomize and send data to combine and sort_array.
*/

int main()
{
	int num = 7;
	int result = doubleFactorial(num);
	char *nick = strdup("Pasione");
	nickMaker(nick);
	free(nick);
	return 0;
}

/**
 * @param num - some number;
 * @return normal exit is value of factorial;
*/

/**
Function doubleFactorial: find double factorial.
*/

int doubleFactorial(int num)
{
	int factorial;

	if ((num % 2) == 0) {
		factorial = 2;
	} else {
		factorial = 1;
	}
	for (int i = factorial + 2; i <= num; i += 2) {
		factorial = factorial * i;
	}
	return factorial;
}

/**
 * @param nick - some word;
*/

/**
Function nickMaker: find double factorial.
*/

void nickMaker(char *nick)
{
	for (int i = 0; i < SIZE; i++) {
		if (nick[i] == 'a' || nick[i] == 'A') {
			nick[i] = '@';
		} else if (nick[i] == 'i' || nick[i] == 'I') {
			nick[i] = '1';
		} else if (nick[i] == 'o' || nick[i] == 'O') {
			nick[i] = '0';
		} else if (nick[i] == 's' || nick[i] == 'S') {
			nick[i] = '$';
		}
	}
}
