#include "lib.h"
/**
 * @file main.c
 * @return успішний код повернення з програми (0).
 * @brief Show the directory.
 * @author Zozulia Igor.
 * @date 07-dec-2020
 * @version 1.0
 */

/**
 * @param argc - count of parameters
 * @param argv - name of paraneter
 * @return успішний код повернення з програми (0).
*/

/**
Function main: call function showFileStruct.
*/

int main(int argc, char **argv)
{
	printf("argc = %d\n", argc);
	FILE *fp;
	if ((fp = freopen(argv[1], "r", stdin)) == NULL) {
		printf("%s\n", "Error opening file.");
		exit(1);
	}
	char *directory = (char *)malloc(256 * sizeof(char));
	printf("%s\n", "Laboratory 14. Author: Zozulia Igor. Show the directory.");
	printf("%s\n", "Enter name of directory using './': ");
	scanf("%256s", directory);
	showFileStruct(directory, 2);
	free(directory);
	fclose(fp);
	return 0;
}
