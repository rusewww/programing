#include "lib.h"

/**
* @file lib.c
* @brief Realization of function.
* @author Zozulia Igor.
* @date 07-dec-2020
* @version 1.0
*/

/**
 * @param directory - name of directory;
 * @param spaceCount - count of space;
 */

/**
function showFileStruct: show file structure.
*/

void showFileStruct(char *directory, int spaceCount)
{
	DIR *folder;

	folder = opendir(directory);
	if (folder != NULL) {
		struct dirent *entry;
		while ((entry = readdir(folder))) {
			if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
				printf("%*c", spaceCount, ' ');
				printf("%s\n", entry->d_name);
				char *fullIneName = (char *)calloc(strlen(directory) + 1 + strlen(entry->d_name), sizeof(char) + 1);
				strncat(fullIneName, directory, strlen(directory) + strlen(entry->d_name));
				strncat(fullIneName, "/", strlen(directory) + strlen(entry->d_name));
				strncat(fullIneName, entry->d_name, strlen(directory) + strlen(entry->d_name));
				showFileStruct(fullIneName, spaceCount + 1);
				free(fullIneName);
			}
		}
	}

	closedir(folder);
}
