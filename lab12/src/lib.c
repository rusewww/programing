#include "lib.h"

/**
* @file lib.c
* @brief Realization of function.
* @author Zozulia Igor.
* @date 07-dec-2020
* @version 1.0
*/

/**
 * @param mass1 - array one;
 * @param mass2 - array two;
 * @param res - result;
 * @param n - size of first;
 * @param m - size of second;
*/

/**
function combine: combine two arrays.
*/

void combine(const int *mass1, const int *mass2, int *res, int n, int m)
{
	int j = 0;
	for (int i = 0; i < (n + m); i++) {
		if (i < n) {
			*(res + i) = mass1[i];
		} else {
			*(res + i) = mass2[j];
			j++;
		}
	}
}

/**
 * @param arr - array one;
 * @param n - size of first;
*/

/**
function sort_array: sort array.
*/

void sort_array(int n, int *arr)
{
	for (int i = 0; i < n - 1; i++) {
		for (int j = 0; j < n - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}