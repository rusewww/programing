#include "lib.h"
/**
 * @file main.c
 * @brief Interaction with the user by the mechanism of input and output.
 * @author Zozulia Igor.
 * @date 07-dec-2020
 * @version 1.0
 */

/**
 * @param mass1 - first array;
 * @param mass2 - second array;
 * @param res - result array;
 * @return успішний код повернення з програми (0).
 */

/**
Function main: generate two arrays by interaction with user and send data to combine and sort_array.
*/

int main()
{
	FILE *fp;
	if ((fp = freopen("./assets/input.txt", "r", stdin)) == NULL) {
		printf("%s\n", "Error opening file.");
		exit(1);
	}
	printf("%s\n", "Laboratory 12. Author: Zozulia Igor. Interaction with the user by the mechanism of input and output.");
	printf("%s\n", "Enter count of first array");
	int count;
	scanf("%d", &count);
	printf("Enter %d numbers:\n", count);
	int *mass1 = (int *)malloc((unsigned long)count * sizeof(int));
	for (int i = 0; i < count; i++) {
		scanf("%d", &mass1[i]);
	}
	printf("%s\n", "Enter count of second array");
	int scount;
	scanf("%d", &scount);
	printf("Enter %d numbers:\n", scount);
	int *mass2 = (int *)malloc((unsigned int)scount * sizeof(int));
	for (int i = 0; i < scount; i++) {
		scanf("%d", &mass2[i]);
	}

	int rescount = count + scount;
	int *res = (int *)malloc((unsigned long)rescount * sizeof(int));

	combine(mass1, mass2, res, count, scount);

	sort_array(rescount, res);

	printf("%s\n", "Result of combination arrays: ");

	for (int i = 0; i < rescount; i++) {
		printf("%d ", res[i]);
	}
	free(mass1);
	free(mass2);
	free(res);
	fclose(fp);
	return 0;
}
